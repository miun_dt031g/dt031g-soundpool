package se.miun.roberi.dt031g.soundpool;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    // All animals used in this app
    private enum Animal {CAT, COW, DOG, LION, MONKEY, SHEEP};

    // To map each animals sound with its loaded sound id from the SoundPool
    private Map<Animal, Integer> soundIds;

    // A SoundPool to play the sounds with
    private SoundPool soundPool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // call the superclass method first
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_cat).setOnClickListener(view -> play(Animal.CAT));
        findViewById(R.id.button_cow).setOnClickListener(view -> play(Animal.COW));
        findViewById(R.id.button_dog).setOnClickListener(view -> play(Animal.DOG));
        findViewById(R.id.button_lion).setOnClickListener(view -> play(Animal.LION));
        findViewById(R.id.button_monkey).setOnClickListener(view -> play(Animal.MONKEY));
        findViewById(R.id.button_sheep).setOnClickListener(view -> play(Animal.SHEEP));

        soundIds = new HashMap<>();
    }

    @Override
    protected void onStart() {
        // call the superclass method first
        super.onStart();

        setupSoundPool();
    }

    private void setupSoundPool() {
        if (soundPool == null) {

            /*
            * From API level 21 the way to create a new SoundPool changed. Instead of using the
            * the constructor, we should now use SoundPool.Builder for level 21+. If minSdkVersion
            * in the build.gradle is lover than 21, you need to do the check below in order to create
            * the SoundPool the correct way. If your minSdkVersion is 21 or higher you only use the
            * SoundPool.Builder.
            */

            // Create the SoundPool
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundPool = new SoundPool.Builder()
                        .setMaxStreams(3) // number of simultaneous streams that can be played
                        .setAudioAttributes(new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION) // use sonification since our sounds has to do with user interface sounds
                                 .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                                 .build()) // build the audio attributes
                        .build(); // build the sound pool
            }
            else {
                soundPool = new SoundPool(
                        3, // number of simultaneous streams that can be played
                        AudioManager.STREAM_MUSIC, // the audio stream type
                        0); // the sample-rate converter quality. Currently has no effect. Use 0 for the default.
            }

            // Load the sounds and store the returned sound id in the map (replacing existing sound id if present)
            soundIds.put(Animal.CAT, soundPool.load(this, R.raw.cat, 1));
            soundIds.put(Animal.COW, soundPool.load(this, R.raw.cow, 1));
            soundIds.put(Animal.DOG, soundPool.load(this, R.raw.dog, 1));
            soundIds.put(Animal.LION, soundPool.load(this, R.raw.lion, 1));
            soundIds.put(Animal.MONKEY, soundPool.load(this, R.raw.monkey, 1));
            soundIds.put(Animal.SHEEP, soundPool.load(this, R.raw.sheep, 1));

            /*
            * Loading sounds can be time consuming. If it is important that all sounds are loaded
            * before using them, we can register a listener for when a sound is loaded. We need to
            * keep track how many sounds of all are loaded.
            */
            soundPool.setOnLoadCompleteListener((soundPool, sampleId, status) -> {
                // we do not care if the sounds are loaded or not, the user simply
                // has to click the button again to here the sound.
                // TODO: remove this completely (only as an example)
            });
        }
    }

    private void play(Animal animal) {
        /*
         * To play a sound in a SoundPool we need the id of the sound we want to play.
         * Arguments to the load method:
         * soundID – a soundID returned by the load() function
         * leftVolume – left volume value (range = 0.0 to 1.0)
         * rightVolume – right volume value (range = 0.0 to 1.0)
         * priority – stream priority (0 = lowest priority)
         * loop – loop mode (0 = no loop, -1 = loop forever)
         * rate – playback rate (1.0 = normal playback, range 0.5 to 2.0)
         */
        soundPool.play(soundIds.get(animal), 1f, 1f, 1, 0, 1f);
    }

    @Override
    protected void onStop() {
        // call the superclass method first
        super.onStop();

        // release resources held by the SoundPool
        if (soundPool != null) {
            soundPool.release();
            soundPool.setOnLoadCompleteListener(null); // not needed if you do not use a listener
            soundPool = null;
        }
    }
}